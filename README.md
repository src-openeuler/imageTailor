# imageTailor

#### 介绍
裁剪ISO工具

#### 软件架构
x86_64/aarch64

#### 安装教程

yum install imageTailor

#### 使用说明

cd /opt/imageTailor
./mkdliso -h

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
